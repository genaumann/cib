"""
Functions for cib-var
"""

import os
import cib.config
import gitlab
import gitlab.exceptions

from typing import Dict

VarsConfigType = Dict[str, Dict[str, Dict[str, str]]]


def generate_vars() -> VarsConfigType:
    """Generate GitLab CI variables for predefined pipeline variables

    Returns:
        VarsConfigType: variable configuration block
    """

    vars_config = {"variables": {"BUILD_IMAGE": {"description": "Ad hoc image build"}}}

    config = cib.config.Config().config

    images = [
        key
        for key, value in config["images"].items()
        if not value.get("when") == "never"
    ]
    vars_config["variables"]["BUILD_IMAGE"]["options"] = images
    vars_config["variables"]["BUILD_IMAGE"]["value"] = images[0]

    return vars_config


def commit_vars(vars_config: str) -> int | str:
    """Execute commit of variables definition

    Args:
        vars_config (str): variable configuration block

    Returns:
        int | str: commit id
    """

    gl = gitlab.Gitlab(
        url=os.environ["CI_SERVER_URL"], private_token=os.environ["CIB_TOKEN"]
    )
    var_file = ".gitlab-ci-vars.yaml"

    # set project
    project = gl.projects.get(os.environ["CI_PROJECT_ID"], lazy=True)

    # default action is update
    action = "update"
    git_file_content = ""
    file_content = "# This file is updated automatically\n" + vars_config
    try:
        git_file_content = (
            project.files.get(var_file, os.environ["CI_DEFAULT_BRANCH"])
            .decode()
            .decode("utf-8")
        )
    except gitlab.exceptions.GitlabGetError:
        # action create if file not exists
        action = "create"

    if file_content == git_file_content:
        return 0

    # build commit
    commit_data = {
        "branch": os.environ["CI_DEFAULT_BRANCH"],
        "commit_message": os.environ.get("CIB_COMMIT_MSG", f"Update {var_file}"),
        "actions": [
            {
                "action": action,
                "file_path": var_file,
                "content": file_content,
            }
        ],
    }

    # create commit
    commit = project.commits.create(commit_data)
    return commit.get_id()
