# Container Image Builder

This Python script generates a GitLab CI/CD pipeline configuration for building Container Images based on a configuration file.
Now you can easily build multiple container images without any knowledge of configuring the GitLab CI/CD pipeline.
You just have to write a `Containerfile` and put it into a configuration file.

## Features

* generate GitLab CI/CD pipeline configuration from a easy configuration file
  * lint `Containerfile`
  * prebuild
  * security scan by `trivy` (optional)
  * push to multiple container registries
* build multiple container image versions from one containerfile
* multiarch container build with `docker buildx`

## Configuration

Place the configuration file `images.yml` in the root directory of your git project.

### Configuration keys

| Key                               | Type       | Default               | Description                                                                                |
| --------------------------------- | ---------- | --------------------- | ------------------------------------------------------------------------------------------ |
| `registry`                        | dict       | {}                    | Set registries where images should be pushed                                               |
| `registry_path`                   | str        | ''                    | Set registry path where final images should be pushed                                      |
| `registry_path_prebuild`          | str        | ''                    | Set registry path where prebuild images should be pushed                                   |
| `images`                          | dict       | {}                    | **REQUIRED** - Specify images as keys                                                      |
| `images.x.containerfile`          | str        | ''                    | **REQUIRED** - Specify the path of the containerfile                                       |
| `images.x.build_args`             | dict       | {}                    | Specify build args for build process                                                       |
| `images.x.security`               | list, bool | [HIGH, CRITICAL]      | When should the security scan fail? Set to `false` to disable                              |
| `images.x.schedule`               | bool       | true                  | Build on scheduled pipeline                                                                |
| `images.x.when`                   | list, str  | [dir/of/dockefile/**] | When should the build job run? Set to `never` to disable building                          |
| `images.x.registry`               | dict       | {}                    | *REQUIRED IF NO GLOBAL `registry`* - overrides global registry                             |
| `images.x.registry_path`          | str        | ''                    | *REQUIRED IF NO GLOBAL `registry_path`* - overrides global registry_path                   |
| `images.x.registry_path_prebuild` | str        | ''                    | *REQUIRED IF NO GLOBAL `registry_path_prebuild`* - overrides global registry_path_prebuild |
| `images.x.arch`                   | list       | []                    | Set arch platform(s) (See [Docker Docs][buildx-platform])                                  |
| `job_config`                      | dict       | {}                    | Modify default job configuration (GitLab CI/CD job configuration)                          |
| `job_config.lint`                 | dict       | {}                    | Override default lint configuration                                                        |
| `job_config.build`                | dict       | {}                    | Override default build configuration                                                       |
| `job_config.prebuild`             | dict       | {}                    | Override default prebuild configuration                                                    |
| `job_config.x.image`              | dict, str  | ''                    | Override image configuration                                                               |
| `job_config.x.tags`               | list       | []                    | Add tag to use a specific GitLab Runner                                                    |

```yaml
---
# push all specified images to these registries
registry:
  # alias: registry address
  alias: registry.example.com
  example: registry2.example.com

# push all specified images to this path inside the registries (final images)
registry_path: foo/bar

# push all specified images to this path inside the registries (prebuild images)
registry_path_prebuild: foo/bar/prebuild

# specify images
images:
  # image name with tag
  image-name:1.0.0:
    # path must be inside the git repo
    containerfile: path/to/containerfile # REQUIRED!
    # build args
    build_args:
      KEY: VALUE
      KEY2: VALUE2
    # security levels of trivy
    security: # default values
      - HIGH
      - CRITICAL
      # - UNKNOWN
      # - LOW
      # - MEDIUM
    # build on scheduled pipeline?
    schedule: True # Default
    # when should this job run?
    when:
      - path/to/file # when this file changed
      - path/to/dir/** # when a file inside this dir changed
    # when: never # <-- never run this job
    # override global registry configuration (only this image will be pushed to this repository)
    registry:
      another: reg2.example.com
    # override global registry_path
    registry_path: another/path
    # override global registry_path_prebuild
    registry_path_prebuild: another/prebuild/path
    # build image for multiple arches
    arch:
      - linux/arm64/v8
      - linux/amd64
```

### Configuration examples

|               |                                       |
| ------------- | ------------------------------------- |
| Image Name    | `reg.example.com/tests/testimage:1.0` |
| Containerfile | `images/testimage`                    |

```yaml
---
images:
  testimage:1.0:
    containerfile: images/testimage/containerfile
    registry:
      reg: reg.example.com
    registry_path: tests
```

|               |                                       |
| ------------- | ------------------------------------- |
| Image Name    | `reg.example.com/tests/testimage:1.0` |
| Containerfile | `images/testimage`                    |
| Build Args    | FOO=bar , KEY=value                   |

```yaml
---
registry:
  reg: reg.example.com
registry_path: tests
images:
  testimage:1.0:
    containerfile: images/testimage/containerfile
    build_args:
      FOO: bar
      KEY: value
```

|                  |                                                   |
| ---------------- | ------------------------------------------------- |
| Image Name       | `reg.example.com/tests/testimage:1.0`             |
| Containerfile    | `images/testimage`                                |
| Run when changed | `files/cert/*.crt`, `files/dep/**`, containerfile |

```yaml
---
images:
  testimage:1.0:
    containerfile: images/testimage/containerfile
    registry:
      reg: reg.example.com
    registry_path: tests
    when:
      - files/cert/*.crt
      - files/dep/**
      - images/testimage/containerfile
```

|               |                                                                               |
| ------------- | ----------------------------------------------------------------------------- |
| Image Name    | `reg.example.com/tests/testimage:1.0`, `reg2.example.com/tests/testimage:1.0` |
| Containerfile | `images/testimage`                                                            |

```yaml
---
images:
  testimage:1.0:
    containerfile: images/testimage/containerfile
    registry:
      reg: reg.example.com
      regtwo: reg2.example.com
    registry_path: tests
```

|               |                                       |
| ------------- | ------------------------------------- |
| Image Name    | `reg.example.com/tests/testimage:1.0` |
| Containerfile | `images/testimage`                    |
| Arch          | `linux/arm64/v8`, `linux/amd64`       |

```yaml
---
images:
  testimage:1.0:
    containerfile: images/testimage/containerfile
    registry:
      reg: reg.example.com
    registry_path: tests
    arch:
      - linux/arm64/v8
      - linux/amd64
```

## TLS certificate

If you use self signed certificates, create the CI/CD variable `ROOT_CA` with **File Type** in the CI/CD settings of you GitLab project.
The content should be the certificate chain of your registry.

The certificate is installed in the Trust Store within the jobs.

## Registry Authentication

In the configuration file you define a registry by the following syntax: `alias: registry`.
This script uses the registry aliases to get the username and password via environment variables.

If you define the following registries:

```yaml
---
registry:
  foo: registry.example.com
  bar: registry2.example.com
```

You have to define the following environment variables:

* `FOO_USER` (username of registry.example.com)
* `FOO_PASSWORD` (password of registry.example.com)
* `BAR_USER` (username of registry2.example.com)
* `BAR_PASSWORD` (password of registry2.example.com)

## Multiarch Builds

You can define multiple image architectures.
For example, if you define `linux/amd64` and `linux/arm64`, the images will be available for these platforms.

Under to hood, the tool calls `docker buildx` with the parameter `--platform`.
The script generates a job configuration with the Docker-In-Docker service so that the GitLab Runner must be setup in **privileged mode**.

More Information: [Use the Docker executor with Docker-in-Docker][gitlab-dind]

### Buildx Configuration

The Variable `$BUILDX_TOML` can be created as **File Type**.
There you can define the [`buildx` configuration][buildx-conf].

If you have a private registry with self signed certificate, create the variable with file type:

```toml
[registry."registry.example.com"]
  ca=["/etc/ssl/certs/ca-certificates.crt"] # this path is fine!
```

[Don't forget to define the `ROOT_CA` variable first.](#tls-certificate)

[buildx-platform]: https://docs.docker.com/engine/reference/commandline/buildx_build/#platform
[gitlab-dind]: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-docker-in-docker
[buildx-conf]: https://docs.docker.com/build/buildkit/toml-configuration/
