"""
Job generation
"""

import os
import cib.config
import cib.utils
import json

IMAGES = {
    "hadolint": "hadolint/hadolint:latest-alpine",
    "trivy": "aquasec/trivy:latest",
    "buildx": "ezkrg/buildx:v0.11.2",
    "kaniko": "gcr.io/kaniko-project/executor:v1.23.2-debug",
    "docker_dind": "docker:27-dind",
}


class Jobs:
    """Generation class"""

    DEFAULT_PIPELINE = {"stages": ["lint", "pre_build", "security", "build"]}

    PIPELINE_SOURCE = os.environ.get("CI_PIPELINE_SOURCE")

    CONFIG = cib.config.Config().config

    def __init__(self) -> None:
        """Init function for Generator class"""

        self.pipeline = (
            self.DEFAULT_PIPELINE
            | self.lint()
            | self.security()
            | self.build(prebuild=True)
            | self.build()
        )

    def lint(self):
        """Generate lint job"""

        def default_config(file: str) -> dict:
            """Generate default pipeline config

            Args:
                file (str): file to lint

            Returns:
                dict: default config
            """

            return {
                "stage": "lint",
                "script": f"hadolint {file}",
                "rules": [
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "schedule"',
                        "when": "never",
                    },
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "web" && $BUILD_IMAGE',
                        "when": "never",
                    },
                    {"when": "always"},
                ],
                "image": IMAGES["hadolint"],
            }

        containerfiles = list(
            set(
                [config["containerfile"] for _, config in self.CONFIG["images"].items()]
            )
        )

        merged_config = {}
        lint_config = self.CONFIG.get("job_config", {}).get("lint", {})
        for file in containerfiles:
            merged_config[f"lint-{file}"] = default_config(file) | lint_config
        return merged_config

    def security(self):
        """Generate security job"""

        def default_config(image_name: list, name: str, image_config: dict) -> dict:
            """Generate default config for security job

            Args:
                image_name (str): generated image name
                name (str): key name of image
                image_config (dict): image config

            Returns:
                dict: default job config
            """

            username, password = cib.utils.get_auth_var(
                image_config, self.CONFIG.get("registry")
            )
            default_security = ["HIGH", "CRITICAL"]
            needs = [f"prebuild-{name}"]

            conf = {
                "stage": "security",
                "variables": {
                    "GIT_STRATEGY": "none",
                    "TRIVY_CACHE_DIR": ".trivycache/",
                },
                "before_script": [
                    "time trivy clean --all",
                    "time trivy image --download-db-only",
                ],
                "script": [
                    f"time trivy image --exit-code 0 --format template --template '@/contrib/gitlab.tpl' --output $CI_PROJECT_DIR/gl-container-scanning-report.json {image_name[0]}",
                    f"time trivy image --exit-code 0 {image_name[0]}",
                    f"time trivy image --exit-code 1 --severity {','.join(image_config.get('security', default_security))} {image_name[0]}",
                ],
                "image": {"name": IMAGES["trivy"], "entrypoint": [""]},
                "cache": {"paths": [".trivycache/"]},
                "artifacts": {
                    "reports": {
                        "container_scanning": "gl-container-scanning-report.json"
                    }
                },
                "rules": [
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "merge_request_event"',
                        "needs": needs,
                    },
                    {"if": f'"{self.PIPELINE_SOURCE}" == "schedule"', "needs": needs},
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "web" && $BUILD_IMAGE',
                        "when": "never",
                    },
                    {"if": "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH", "needs": needs},
                ],
            }

            if username:
                conf["variables"]["TRIVY_USERNAME"] = username
            if password:
                conf["variables"]["TRIVY_PASSWORD"] = password

            return conf

        merged_config = {}
        new_images = cib.utils.get_new_images()
        for name, config in self.CONFIG["images"].items():
            # skip if security is False
            if config.get("security") is False:
                continue
            # skip if image is skipped
            if config.get("when") == "never":
                continue

            image_name = cib.utils.get_image_name(self.CONFIG, name, config, True)

            def_config = default_config(image_name, name, config)

            # if when is list - set changes to rule
            if isinstance(config.get("when"), list):
                def_config["rules"][0] = def_config["rules"][0] | {
                    "changes": config["when"]
                }
                def_config["rules"][3] = def_config["rules"][3] | {
                    "changes": config["when"]
                }
            # if when is none, use containerfile as fallback
            elif config.get("when") is None and name not in new_images:
                d = os.path.dirname(config["containerfile"])
                def_config["rules"][0] = def_config["rules"][0] | {
                    "changes": [f"{d}/**"]
                }
                def_config["rules"][3] = def_config["rules"][3] | {
                    "changes": [f"{d}/**"]
                }

            # skip schedule rule if schedule is False
            if config.get("schedule") is False:
                def_config["rules"][1] = def_config["rules"][1] | {"when": "never"}

            merged_config[f"trivy-{name}"] = def_config | self.CONFIG.get(
                "job_config", {}
            ).get("trivy", {})

        return merged_config

    def build(self, prebuild: bool = False):
        """Generate build job

        Args:
            prebuild (bool): prebuild job? (default True)

        """

        def default_config(config: dict, image_name: str, prebuild: bool) -> dict:
            """Generate prebuild default config

            Returns:
                dict: default config
            """

            needs = [f"lint-{config['containerfile']}"]

            # add prebuild job as dependency if build
            if not prebuild:
                needs.append(f"prebuild-{name}")

            # add security job as dependency if security and build
            if not prebuild and (
                config.get("security") is None or config.get("security") is not False
            ):
                needs.append(f"trivy-{name}")

            auth_json, auth_scripts = cib.utils.generate_auth(
                config, self.CONFIG.get("registry")
            )
            # add before_script
            dumped_auth = json.dumps(auth_json)
            before_script = [
                *auth_scripts,
                f'echo {json.dumps(dumped_auth)} > {"~/.docker/config.json" if config.get("arch") else "/kaniko/.docker/config.json"}',
            ]
            # add cert if ROOT_CA is specified
            if os.environ.get("ROOT_CA"):
                before_script.append(
                    f'cat "$ROOT_CA" >> {"/etc/ssl/certs/ca-certificates.crt" if config.get("arch") else "/kaniko/ssl/certs/ca-certificates.crt"}'
                )

            # add docker dir if multiarch
            if config.get("arch"):
                before_script.insert(0, "mkdir -p ~/.docker")
                image = IMAGES["buildx"]
                services = [IMAGES["docker_dind"]]
            else:
                image = {
                    "name": IMAGES["kaniko"],
                    "entrypoint": [""],
                }
                services = []

            return {
                "stage": "pre_build" if prebuild else "build",
                "image": image,
                "services": services,
                "rules": [
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "merge_request_event"',
                        "needs": needs,
                    },
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "schedule"',
                    },
                    {
                        "if": f'"{self.PIPELINE_SOURCE}" == "web" && $BUILD_IMAGE == "{image_name}"',
                        "when": "never" if prebuild else "on_success",
                    },
                    {
                        # fallback rule if $BUILD_IMAGE != image_name
                        "if": f'"{self.PIPELINE_SOURCE}" == "web" && $BUILD_IMAGE',
                        "when": "never",
                    },
                    {
                        "if": "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH",
                        "when": (
                            "never"
                            if prebuild and config.get("security") is False
                            else "on_success"
                        ),
                        "needs": needs,
                    },
                ],
                "before_script": before_script,
            }

        new_images = cib.utils.get_new_images()
        merged_config = {}
        for name, config in self.CONFIG["images"].items():
            def_config = default_config(config, name, prebuild)

            # skip job when == never
            if config.get("when") == "never":
                continue
            elif isinstance(config.get("when"), list):
                # extend default rules with when changes
                def_config["rules"][0] = def_config["rules"][0] | {
                    "changes": config["when"]
                }
                def_config["rules"][4] = def_config["rules"][4] | {
                    "changes": config["when"]
                }
            elif config.get("when") == None and name not in new_images:
                d = os.path.dirname(config["containerfile"])
                def_config["rules"][0] = def_config["rules"][0] | {
                    "changes": [f"{d}/**"]
                }
                def_config["rules"][4] = def_config["rules"][4] | {
                    "changes": [f"{d}/**"]
                }

            if not prebuild:
                def_config["rules"][0] = def_config["rules"][0] | {"when": "never"}

            # skip schedule if False
            if config.get("schedule") is False:
                def_config["rules"][1] = def_config["rules"][1] | {"when": "never"}

            # set image names
            image_names = cib.utils.get_image_name(self.CONFIG, name, config, prebuild)

            # set build args
            build_args = [
                f'--build-arg {key}="{value}"'
                for key, value in config.get("build_args", {}).items()
            ]

            if config.get("arch"):
                script = [
                    f'docker buildx create --use {"--config $BUILDX_TOML" if os.environ.get("BUILDX_TOML") else ""}',
                    f'docker buildx build -f {config["containerfile"]} --platform {" --platform ".join(config.get("arch"))} {" ".join(build_args)} -t {" -t ".join(image_names)} {"" if config.get("security") is False and prebuild else "--push"} .',
                ]
            else:
                # no push if prebuild and no security scan
                no_push = ""
                if config.get("security") is False and prebuild:
                    no_push = "--no-push"

                # set script string
                script = f'/kaniko/executor --context "$CI_PROJECT_DIR" --destination {" --destination ".join(image_names)} --dockerfile {config["containerfile"]} {" ".join(build_args)} {no_push}'

            tool = "buildx" if config.get("arch") else "kaniko"
            job_type = "prebuild" if prebuild else "build"
            job_type_slug = f"prebuild_{tool}" if prebuild else f"build_{tool}"
            merged_config[f"{job_type}-{name}"] = (
                def_config
                | self.CONFIG.get("job_config", {}).get(job_type_slug, {})
                | {"script": script}
            )

        return merged_config
