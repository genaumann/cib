import setuptools
from cib import NAME, VERSION

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name=NAME,
    version=VERSION,
    author="Gino Naumann (SVA GmbH)",
    description="Build Container images in GitLab CI/CD",
    long_description=long_description,
    long_description_content_type="text/markdown",
    entry_points={"console_scripts": ["cib = cib.cli:run", "cib-vars = cib.cli:vars"]},
    url="https://gitlab.com/genaumann/cib",
    packages=[NAME],
    include_package_data=True,
    python_requires=">=3.11",
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["pyyaml", "python-gitlab"],
)
