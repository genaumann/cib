"""
Build Container images in GitLab CI/CD
"""

NAME = "cib"
VERSION = "0.0.0"

__version__ = VERSION
