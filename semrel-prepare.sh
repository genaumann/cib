#!/bin/sh

######################
# Update VERSION
######################

echo "${1}" > VERSION
sed -i "s/cib:[0-9]*\.[0-9]*\.[0-9]*/cib:${1}/" images.yml

if [ "${2}" ] ; then
    sed -i "s/VERSION = \"${2}\"/VERSION = \"${1}\"/g" cib/__init__.py
fi
