# Changelog

## [1.0.6](https://gitlab.com/genaumann/cib/compare/v1.0.5...v1.0.6) (2024-07-30)


### 🐛 Bugfix 🐛

* add trivy clean subcommand ([0ba85b0](https://gitlab.com/genaumann/cib/commit/0ba85b0ebe67ee92cc1d9838997ce02e293abac6))
* don't run prebuild on main when no security ([bcd302c](https://gitlab.com/genaumann/cib/commit/bcd302c214849773451cb14ca51a05b50bff8985))


### 💿 Continuous Integration 💿

* **gitlab:** use genaumann pytools image ([600bf60](https://gitlab.com/genaumann/cib/commit/600bf604c61ed92a391c7cb1f3e271d1abd44f05))


### 🖌️ Style 🖌️

* apply pylint suggestions ([634006d](https://gitlab.com/genaumann/cib/commit/634006d6272ba81306e7ffadf95bdcf716a03854))


### 🏗️ Build 🏗️

* update to python 3.12 ([18cf268](https://gitlab.com/genaumann/cib/commit/18cf2689f351ef20d7bfa3e1862e7396733c06b8))

## [1.0.5](https://gitlab.com/genaumann/cib/compare/v1.0.4...v1.0.5) (2023-11-26)


### 🐛 Bugfix 🐛

* **jobs:** dont push on MR pipeline ([fbda3e0](https://gitlab.com/genaumann/cib/commit/fbda3e0c2f7e135b50509f7f8559bccd0752d456)), closes [#2](https://gitlab.com/genaumann/cib/issues/2)
* **vars:** decode error ([d08504b](https://gitlab.com/genaumann/cib/commit/d08504bbd0c241b36edd12d32850c72970c806fd)), closes [#3](https://gitlab.com/genaumann/cib/issues/3)


### 💿 Continuous Integration 💿

* **gitlab:** use new semantic-release image ([f5a0662](https://gitlab.com/genaumann/cib/commit/f5a066292c427266f685fcef50176e72b53f4a68))

## [1.0.4](https://gitlab.com/genaumann/cib/compare/v1.0.3...v1.0.4) (2023-11-23)


### 💿 Continuous Integration 💿

* **release:** add build note section [skip ci] ([dae4501](https://gitlab.com/genaumann/cib/commit/dae450155a0505764dcdacc84e38f3447af20986))

## [1.0.3](https://gitlab.com/genaumann/cib/compare/v1.0.2...v1.0.3) (2023-11-23)

## [1.0.2](https://gitlab.com/genaumann/cib/compare/v1.0.1...v1.0.2) (2023-11-23)


### 💿 Continuous Integration 💿

* **gitlab:** add mr test deployment pipeline ([f3bf9c6](https://gitlab.com/genaumann/cib/commit/f3bf9c6afda9c66d06af90c69ae2ac824395005a))
* **gitlab:** add workflow config [skip ci] ([a208d77](https://gitlab.com/genaumann/cib/commit/a208d773bb03db2390ff95f2500a7af32a0c0054))

## [1.0.1](https://gitlab.com/genaumann/cib/compare/v1.0.0...v1.0.1) (2023-11-23)


### 💿 Continuous Integration 💿

* **gitlab:** fix needs dependency ([5d5a53a](https://gitlab.com/genaumann/cib/commit/5d5a53ae29c40cedb4a066f78b1ae2cdae41f170))

## 1.0.0 (2023-11-23)


### ✨ Feature ✨

* add cli entrypoint ([da0ffe4](https://gitlab.com/genaumann/cib/commit/da0ffe46e47d4596e71a91238d0b8c21aebd4c14))
* **build:** add job for prebuild and build ([9345d3d](https://gitlab.com/genaumann/cib/commit/9345d3dc5d47f61f6e19e68f3759f4f674ee52d4))
* **build:** add security job as need ([63ca21f](https://gitlab.com/genaumann/cib/commit/63ca21f9f7c5d22ecf8fdaef670a7625d788d40f))
* **build:** no push if prebuild and no security ([8155e60](https://gitlab.com/genaumann/cib/commit/8155e60c3817ba046a2e545bae4d1480a398e146))
* **cli:** save pipeline config in file ([a99e4a3](https://gitlab.com/genaumann/cib/commit/a99e4a378710770fcf1d7582dc5654bf86febf2f))
* **config:** add arch ([49d2ece](https://gitlab.com/genaumann/cib/commit/49d2ece0b009634f8cc94299dd22292178844bf5))
* **config:** add job config ([624759c](https://gitlab.com/genaumann/cib/commit/624759c64c1334475cfa4a202ff3f4f8931779f8))
* **config:** add registry_path ([ecff1ae](https://gitlab.com/genaumann/cib/commit/ecff1ae1eb6037755201c8ce56e158e15f091bc4))
* **config:** read and check config file ([c73e3b4](https://gitlab.com/genaumann/cib/commit/c73e3b48062e9dda085e166167433a0599ff0180))
* glob change where dockerfile is located ([c295528](https://gitlab.com/genaumann/cib/commit/c295528e22e76193cddba3873c34b36c324a4c90))
* **jobs:** add job when image is added ([ddefa52](https://gitlab.com/genaumann/cib/commit/ddefa52a9eb0858b70efad66ad380397d87bbe7b))
* **jobs:** modify rules for BUILD_IMAGE var ([76827f9](https://gitlab.com/genaumann/cib/commit/76827f9dadca952d02ef6ed069ac5e6859a1bc9b))
* **lint:** generate lint job ([4c40848](https://gitlab.com/genaumann/cib/commit/4c40848106e90a8b2ac9dfdfd750d8d1fb3e8cf7))
* **multiarch:** add docker buildx ([1fc8d58](https://gitlab.com/genaumann/cib/commit/1fc8d5866e90ce5e5b055ac12b5486e303fae044))
* **security:** add job ([147b863](https://gitlab.com/genaumann/cib/commit/147b863f3088f3bb80ccf4e6b089de9bc6a9abc3))
* **utils:** add generate auth ([9d7ea6f](https://gitlab.com/genaumann/cib/commit/9d7ea6f99e8dca062d1ea969ca496644d673dfbc))
* **utils:** add get_image_name function ([efb5cd5](https://gitlab.com/genaumann/cib/commit/efb5cd5d4ed6e1dd036e66a04baa16034b6895ed))
* **vars:** commit if file changed ([7ab196f](https://gitlab.com/genaumann/cib/commit/7ab196fb955349c41909b39cb90efc034e6d2fc2)), closes [#6](https://gitlab.com/genaumann/cib/issues/6)
* **vars:** create and commit ([334fabd](https://gitlab.com/genaumann/cib/commit/334fabda2276730e63cabbf64e5988499805e964))


### 🐛 Bugfix 🐛

* **build:** add -p to mkdir ([af96828](https://gitlab.com/genaumann/cib/commit/af968281763b7a81d01bd4d24687d8b55aa45059))
* **build:** auth encoding in pipeline ([e8929d5](https://gitlab.com/genaumann/cib/commit/e8929d51f2fc318fbe2ca0b76ab8465547e95ae5))
* **build:** buildx ca certificate path ([c30e265](https://gitlab.com/genaumann/cib/commit/c30e265fb56ac1399e51c6d8b6ea7cb9e5ffcf2c))
* **build:** kaniko containerfile --> dockerfile ([1c3dc67](https://gitlab.com/genaumann/cib/commit/1c3dc67f5999d033468da7c836a732a8a0508f44))
* **build:** registry_path can be / ([2ef5154](https://gitlab.com/genaumann/cib/commit/2ef5154f7b446a4e9bda9df8f47be168d68f6359))
* **build:** variable error in before_script ([38f0d86](https://gitlab.com/genaumann/cib/commit/38f0d86c8f0f590868de0c9f0f773c1bb2f4bf69))
* **config:** change type of job image ([9a15977](https://gitlab.com/genaumann/cib/commit/9a1597745c5b26fa6de2f71ac9c83375de644eb7))
* merge job config ([a2dd489](https://gitlab.com/genaumann/cib/commit/a2dd489fd8a46e5bb6d895fbff4d6b26df8b4658))
* **security:** delete TRIVY vars if unset ([319702e](https://gitlab.com/genaumann/cib/commit/319702eb0d0271605917e81e25ee95d0c930df5b))
* **vars:** exclude images ([c9edee3](https://gitlab.com/genaumann/cib/commit/c9edee392d3c85e9ddb2c513c83f07977570e707)), closes [#7](https://gitlab.com/genaumann/cib/issues/7)


### 💿 Continuous Integration 💿

* **gitlab:** add config ([8c9143b](https://gitlab.com/genaumann/cib/commit/8c9143b4531100aefa824848879f1c49b4b2c8e6))
* **release:** add release config and script ([68487d3](https://gitlab.com/genaumann/cib/commit/68487d3819f401fa07312d03807de80d2f397e90))


### 🖌️ Style 🖌️

* remove duplicate code ([7d6c4e3](https://gitlab.com/genaumann/cib/commit/7d6c4e38adef0cad0bda52c6f1613c50be9f3a86))
* remove german error message ([63cfcf4](https://gitlab.com/genaumann/cib/commit/63cfcf40ade4c78e44c169d89d7d530f5c478a12))


### 📖 Documentation 📖

* add inital README ([79f0985](https://gitlab.com/genaumann/cib/commit/79f0985fece06b29a287eb39a65c429dff6b359c))
* **multiarch:** extend docs ([88c6960](https://gitlab.com/genaumann/cib/commit/88c696019111481775b38df67ed2b405a8b09a64))
