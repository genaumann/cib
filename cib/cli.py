"""
CLI entrypoint
"""

import yaml
import cib.config
import cib.job
import cib.var
import os


def run() -> int:
    """CLI entrypoint

    Returns:
        int: exit code
    """

    # check token is set
    if not os.environ.get("CIB_TOKEN"):
        raise ValueError("Specify GitLab token with api scope in '$CIB_TOKEN'")

    pipeline = cib.job.Jobs().pipeline

    print(yaml.dump(pipeline))
    with open("pipeline.yml", "w", encoding="utf-8") as yamlfile:
        yaml.dump(pipeline, yamlfile)

    return 0


def vars() -> int:
    """CLI entrypoint for cib-vars

    Returns:
        int: exit code
    """

    # check token is set
    if not os.environ.get("CIB_TOKEN"):
        raise ValueError("Specify GitLab token with api scope in '$CIB_TOKEN'")

    var_block = yaml.dump(cib.var.generate_vars())

    print(var_block)

    commit_id = cib.var.commit_vars(var_block)
    if commit_id == 0:
        print("No changes")
    else:
        print(f"Commit ID: {commit_id}")

    return 0 if commit_id else 1
