"""
Config scripts
"""

import os
import yaml


class Config:
    """
    Config class
    """

    CONFIG_SCHEMA = {
        "registry": dict,
        "registry_path": str,
        "registry_path_prebuild": str,
        "images": dict,
        "job_config": dict,
    }

    IMAGE_SCHEMA = {
        "containerfile": str,
        "build_args": dict,
        "security": (bool, list),
        "registry": dict,
        "registry_path": str,
        "registry_path_prebuild": str,
        "schedule": bool,
        "arch": list,
        "when": (str, list),
    }

    JOB_SCHEMA = {"image": (str, dict), "tags": list}

    def __init__(self) -> None:
        """
        Config init function
        """

        self.config = self.parse_config_file()
        self.check(self.config, self.CONFIG_SCHEMA)

    def parse_config_file(self) -> dict:
        """
        Parse yaml config file

        Returns:
            dict: config dict
        """

        file = "images.yml"

        if not os.path.isfile(file):
            raise FileNotFoundError(f'Config file "{file}" not found')

        with open(file, "r", encoding="utf-8") as file_stream:
            config = yaml.safe_load(file_stream)

        return config

    def check(self, config_dict, schema) -> bool:
        """Check config keys"""

        if not isinstance(config_dict, dict):
            raise ValueError("Data needs to be a dict")

        extra_keys = set(config_dict) - set(schema)
        if extra_keys:
            raise ValueError(f"Unexpected key in image config: {', '.join(extra_keys)}")

        for key, expected_type in schema.items():
            if key not in config_dict:
                continue

            value = config_dict[key]

            if key == "images":
                if not isinstance(value, expected_type):
                    raise ValueError(f"'{key}' needs to be a dict")

                for sub_key, sub_value in value.items():
                    self.check(sub_value, self.IMAGE_SCHEMA)
            elif key == "job_config":
                if not isinstance(value, expected_type):
                    raise ValueError(f"'{key}' needs to be a dict")

                for sub_key, sub_value in value.items():
                    self.check(sub_value, self.JOB_SCHEMA)
            else:
                if isinstance(expected_type, tuple):
                    if not any(isinstance(value, typ) for typ in expected_type):
                        types_names = ", ".join(typ.__name__ for typ in expected_type)
                        raise ValueError(
                            f"'{key}' needs to be one of these types: {types_names}"
                        )
                elif isinstance(expected_type, dict):
                    self.check(value, expected_type)
                else:
                    if not isinstance(value, expected_type):
                        raise ValueError(
                            f"'{key}' needs to be type {expected_type.__name__}"
                        )

        return True
