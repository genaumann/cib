"""
Utils
"""

import os
import gitlab
import yaml
import cib.config


def get_image_name(
    config: dict, image_name: str, image_config: dict, prebuild: bool = False
) -> list:
    """Generate image name(s)

    Args:
        config (dict): full config
        image_name (str): image name (dict key of images)
        image_config (dict): image config
        prebuild (bool): is prebuild? (default: False)

    Returns:
        list: full qualified container names
    """

    # set path type
    path = "registry_path"
    prepend_name = ""
    if prebuild:
        path = "registry_path_prebuild"
        prepend_name = "-rc-$CI_PIPELINE_ID"

    image_names = []

    if registry := image_config.get("registry") or config.get("registry"):
        for _, reg in registry.items():
            reg_path = f"/{image_config.get(path, config.get(path))}/"
            # remove slashes
            if reg_path == "///":
                reg_path = "/"
            # raise error if no path is specified
            if not reg_path:
                raise ValueError(f'{path} is not configured on image "{image_name}"')
            image_names.append(f"{reg}{reg_path}{image_name}{prepend_name}")
    else:
        raise ValueError(f"No registry is set on image {image_name}")

    return image_names


def generate_auth(image_config: dict, registry: dict) -> tuple[dict, list]:
    """Generate auth json for docker config

    Args:
        image_config (dict): image config
        registry (dict): all global registries


    Returns:
        tuple: (auth dict, before scripts)
    """

    registries = image_config.get("registry", registry)

    auths = {}
    scripts = []
    for alias, reg in registries.items():
        user_var = f"{alias.upper()}_USER"
        password_var = f"{alias.upper()}_PASSWORD"
        # check user is set
        if not os.environ.get(user_var):
            print(
                f"WARN: User for registry {reg} is missing! Set {alias.upper()}_USER variable"
            )
            continue
        # check password is set
        if not os.environ.get(password_var):
            print(
                f"WARN: Password for registry {reg} is missing! Set {alias.upper()}_PASSWORD variable"
            )
            continue

        auth_var = f"{alias.upper()}_AUTH"
        # get auth config dict
        # the var needs to be created inside the job for security reasons
        auths[reg] = {"auth": f"${auth_var}"}

        # get before script for password generation
        scripts.append(
            f'{auth_var}=$(echo -n "${user_var}:${password_var}" | base64 | tr -d "\\n")'
        )

    return ({"auths": auths}, set(scripts))


def get_auth_var(image_config: dict, registry: dict) -> tuple[str, str]:
    """Get first auth var

    Args:
        image_config (dict): image config
        registry (dict): all global registries

    Returns:
        tuple[str, str]: comma seperated auth vars
    """

    registries: dict = image_config.get("registry", registry)

    for alias in registries:
        user_var = f"{alias.upper()}_USER"
        password_var = f"{alias.upper()}_PASSWORD"

        if not os.environ.get(user_var):
            continue
        if not os.environ.get(password_var):
            continue

        # return at first alias
        return (f"${user_var}", f"${password_var}")

    return (None, None)


def compare_dicts_images(old_config: dict, new_config: dict) -> list[str]:
    """Compare image dict

    Args:
        old_config (dict): old image dict
        new_config (dict): image dict

    Returns:
        list: changed or new images
    """

    diff = []
    # Check for changes or additions in new_config
    for image, details in new_config["images"].items():
        if image not in old_config["images"]:
            # If the image is not present in dict1, it is new
            diff.append(image)
        elif old_config["images"][image] != details:
            # If the image is present but details are different, it is changed
            diff.append(image)
    return diff


def get_new_images() -> list[str]:
    """Get new images as list

    Returns:
        list: image names
    """

    config = cib.config.Config().config

    gl = gitlab.Gitlab(
        url=os.environ["CI_SERVER_URL"], private_token=os.environ["CIB_TOKEN"]
    )
    project = gl.projects.get(os.environ["CI_PROJECT_ID"], lazy=True)

    def comp_content(ref: str) -> list:
        """Compare dict content

        Args:
            ref (str): ref string

        Returns:
            list: different images
        """

        try:
            old_file = project.files.get("images.yml", ref).decode()
        except gitlab.exceptions.GitlabGetError:
            return list(config["images"].keys())

        old_file_yml = yaml.safe_load(old_file.decode("utf-8"))

        return compare_dicts_images(old_file_yml, config)

    if os.environ.get("CI_PIPELINE_SOURCE") == "merge_request_event":
        return comp_content(os.environ["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"])
    else:
        # get last successfull pipeline from branch
        pipelines = project.pipelines.list(
            **{"ref": os.environ["CI_COMMIT_BRANCH"], "status": "success"}
        )
        try:
            latest_pipeline = pipelines[0]
        except IndexError:
            return list(config["images"].keys())
        # now we have the sha of the latest pipeline
        # lets get images.yml from this sha
        return comp_content(latest_pipeline.sha)
